jQuery( document ).ready( function( $ ) {
	let groupSelect = $( '#sync_group_name' );

	groupSelect.change( function( e ) {
		$( '#count' ).show().text( 'In this group  ' + $( this ).find( 'option:selected' ).data( 'user_count' ) + ' users' );
	} );

	/**
	 * Send Ajax sync user group.
	 */
	$( '#sync_form_user_group' ).click( function( e ) {
		e.preventDefault();

		let data = {
			action: 'sync_user_group',
			groupID: groupSelect.val(),
			userCount: groupSelect.find( 'option:selected' ).data( 'user_count' ),
			nonce: pcs.nonce
		};


		/**
		 * Basic Ajax request.
		 */
		$.ajax( {
			type: 'POST',
			url: pcs.url,
			data: data,
			beforeSend: function() {
				$( '.progress_bar_wrapper' ).show();
			},
			success: function( res ) {
				// console.log( res );
				clearInterval( progressBar );
			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			},
		} );


		/**
		 * Auxiliary Ajax request to get data for the progress bar.
		 *
		 * @type {number}
		 */
		let progressBar = setInterval( () => {
			let progress = {
				action: 'get_progress',
				nonce: pcs.nonce
			};

			$.ajax( {
				type: 'POST',
				url: pcs.url,
				data: progress,
				success: function( res ) {
					// console.log( res );
					if ( res.success ) {
						$( '#create' ).text( res.data.create );
						$( '#skip' ).text( res.data.skip );
						progressBarComplete( res.data.create_num, res.data.skip_num );
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				},
			} );
		}, 1000 );
	} );

	/**
	 * Progress bar.
	 *
	 * @param created count created user.
	 * @param skip count skipped user.
	 */
	function progressBarComplete( created, skip ) {
		let currentCount = Number( created ) + Number( skip );

		let totalCount = $( '#sync_group_name' ).find( 'option:selected' ).data( 'user_count' );

		let progress = ( currentCount / totalCount ) * 100;

		$( '.progress-bar' ).width( progress + '%' ).text( Math.round( progress ) + '%' );
	}

} );