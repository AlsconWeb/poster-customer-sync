<?php
/**
 * Created 14.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PCS\Admin
 */

$token = get_option( 'pcs_auth_token', false );

?>
<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<h3><?php esc_html_e( 'Poster token', 'pcs' ); ?></h3>
		</div>
	</div>
	<form class="row g-3" method="post">
		<div class="col-sm-8">
			<label for="pcs-token"><?php esc_html_e( 'Poster token', 'pcs' ); ?></label>
			<input
					type="password"
					class="form-control"
					id="pcs-token"
					name="pcs-token"
					placeholder="<?php esc_attr_e( 'Application secret key', 'pcs' ); ?>"
					value="<?php echo esc_attr( $token ) ?? ''; ?>"
			>
		</div>
		<div class="col-sm-4">
			<?php wp_nonce_field( 'pcs_auth', 'pcs_auth_nonce' ); ?>
			<input
					type="submit"
					autocomplete="off"
					value="<?php esc_html_e( 'Save', 'pcs' ); ?>"
					class="btn btn-primary mt-4"
					name="pcs_auth">
		</div>
	</form>
	<div class="row mt-4">
		<div class="col-12">
			<h3><?php esc_html_e( 'Token check', 'pcs' ); ?></h3>
			<p class="description">
				<?php esc_html_e( 'Click the verify token button to start verification', 'pcs' ); ?>
			</p>
			<?php if ( ! empty( $token ) ) : ?>
				<p style="color: #0e7e12">
					<?php esc_html_e( 'Token received successfully', 'pcs' ); ?>
					<i class="dashicons-before dashicons-saved"></i>
				</p>
			<?php endif; ?>
			<form method="post">
				<div class="col-auto">
					<?php wp_nonce_field( 'pcs_get_token', 'pcs_get_token_nonce' ); ?>
					<input
							type="submit"
							value="<?php esc_html_e( 'Check Token', 'pcs' ); ?>"
							class="btn btn-primary mt-4"
							name="pcs_get_token">
				</div>
			</form>
		</div>
		<?php
		$group = get_transient( 'pcs_user_groups' );
		if ( $group ) :
			?>
			<div class="row mt-4">
				<div class="col-12">
					<h3><?php esc_html_e( 'Select the group you want to sync', 'pcs' ); ?></h3>

					<form action="" method="post">
						<div class="row">
							<div class="col-auto">
								<select
										class="form-select" id="sync_group_name" name="sync_group_name"
										aria-label="<?php esc_html_e( 'Select a user group', 'pcs' ); ?>">
									<option selected
											value="0"><?php esc_html_e( 'Select a user group', 'pcs' ); ?></option>
									<?php foreach ( $group as $item ) : ?>
										<option
												value="<?php echo esc_attr( $item->client_groups_id ); ?>"
												data-user_count="<?php echo esc_attr( $item->count_groups_clients ); ?>">
											<?php echo esc_html( $item->client_groups_name ); ?>
										</option>
									<?php endforeach; ?>
								</select>
								<p class="description" id="count" style="display: none"></p>
							</div>
							<div class="col-3 alignleft">
								<input
										type="submit" class="btn btn-primary mb-3"
										id="sync_form_user_group"
										value="<?php esc_attr_e( 'Sync', 'pcs' ); ?>"
										name="sync_group">
							</div>
						</div>
					</form>

					<div class="progress_bar_wrapper" style="display: none;">
						<p class="alignright ms-2">
							<span id="create"><?php esc_html_e( 'Created: ', 'pcs' ); ?>0</span>
							/
							<span id="skip"><?php esc_html_e( 'Skip: ', 'pcs' ); ?>0</span>
						</p>
						<div class="progress import">

							<div
									class="progress-bar bg-success"
									role="progressbar"
									style="width: 0%"
									aria-valuenow="0"
									aria-valuemin="0"
									aria-valuemax="100">
								0%
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>

