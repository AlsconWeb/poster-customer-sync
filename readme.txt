=== Poster Customer Sync ===
Contributors: alexlavigin
Tags: WooCommerce, woocommerce, poster, Poster, user sync, user synchronization, poster.com
Requires PHP: 7.4
Requires at least: 5.0.0
Tested up to: 5.9
Stable tag: 1.0.0
License: GPL v2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Connect your site to the [Poster] (https://joinposter.com) system, and sync your users to your site.

== Description ==
Make your work with Poster convenient, transfer all discounts and users to your site.

### Plugin features
* will allow you to synchronize users and their data with the Poster system
* with each purchase, the purchase amount is added to the total amount of money spent in the system
* create and link discount cards and discounts to the user

== Installation ==
1. Upload `poster-customer-sync` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.

== Screenshots ==

== Changelog ==

= 1.0.0 =
* Initial release
