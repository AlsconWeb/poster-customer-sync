<?php
/**
 * Poster Customer Sync.
 *
 * @author            Alex L
 * @license           GPL-2.0-or-later
 * @wordpress-plugin
 *
 * Plugin Name:       Poster Customer Sync
 * Plugin URI:        https://i-wp-dev.com/
 * Description:       The plugin makes it possible to synchronize buyers in the Poster system
 * Author:            Alex L
 * Author URI:        https://i-wp-dev.com/
 * Version:           1.0.1
 * Requires at least: 5.6.0
 * Requires PHP:      7.4
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       pcs
 * Domain Path:       /languages/
 *
 * @package           PCS
 */

use PCS\Init\PluginInit;

/**
 * Plugin version.
 */
define( 'PCS_VERSION', '1.0.0a' );

/**
 * Path to the plugin dir.
 */
define( 'PCS_PATH', __DIR__ );

/**
 * Plugin dir url.
 */
define( 'PCS_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) );

/**
 * Plugin prefix.
 */
define( 'PCS_PREFIX', 'pcs_' );

/**
 * Minimum required php version.
 */
define( 'PCS_MINIMUM_PHP_REQUIRED_VERSION', '7.4' );

/**
 * Main plugin file.
 */
define( 'PCS_FILE', __FILE__ );

/**
 * Init plugin on plugin load.
 */
require_once constant( 'PCS_PATH' ) . '/vendor/autoload.php';

if ( ! PluginInit::is_php_version() ) {

	add_action( 'admin_notices', 'PCS\Admin\AdminNotice::php_version_nope' );

	if ( is_plugin_active( plugin_basename( constant( 'PCS_FILE' ) ) ) ) {
		deactivate_plugins( plugin_basename( constant( 'PCS_FILE' ) ) );
		// phpcs:disable WordPress.Security.NonceVerification.Recommended
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}

	return;
}

if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
	add_action( 'admin_notices', 'PCS\Admin\AdminNotice::woocommerce_active' );

	deactivate_plugins( plugin_basename( constant( 'PCS_FILE' ) ) );
	// phpcs:disable WordPress.Security.NonceVerification.Recommended
	if ( isset( $_GET['activate'] ) ) {
		unset( $_GET['activate'] );
	}
}

global $pcs_plugin;

$pcs_plugin = new PluginInit();

/**
 * Add Coron Update.
 */
function add_cron_update() {
	if ( ! wp_next_scheduled( 'update_new_customer' ) ) {
		wp_schedule_event( time(), 'daily', 'update_new_customer' );
	}

	if ( ! wp_next_scheduled( 'update_five_percent_group' ) ) {
		wp_schedule_event( time(), 'daily', 'update_five_percent_group' );
	}

	if ( ! wp_next_scheduled( 'update_seven_percent_group' ) ) {
		wp_schedule_event( time(), 'daily', 'update_seven_percent_group' );
	}

	if ( ! wp_next_scheduled( 'update_ten_percent_group' ) ) {
		wp_schedule_event( time(), 'daily', 'update_ten_percent_group' );
	}

	if ( ! wp_next_scheduled( 'update_fifteen_percent_group' ) ) {
		wp_schedule_event( time(), 'daily', 'update_fifteen_percent_group' );
	}
}

register_activation_hook( __FILE__, 'add_cron_update' );
