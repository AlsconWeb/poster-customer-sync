<?php
/**
 * Created 19.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PCS\Poster
 */

namespace PCS\Poster;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Api class file.
 */
class Api {
	/**
	 * GuzzleHttp Client.
	 *
	 * @var Client GuzzleHttp.
	 */
	protected $http;
	/**
	 * Account ID in poster.a
	 *
	 * @var false|string
	 */
	private $accountID;
	/**
	 * Application ID in poster.
	 *
	 * @var false|string
	 */
	private $applicationID;
	/**
	 * Application Secret in poster.
	 *
	 * @var false|string
	 */
	private $applicationSecret;
	/**
	 * Token in poster.
	 *
	 * @var false|string
	 */
	private $token;

	/**
	 *  Api construct.
	 */
	public function __construct() {
		$this->http = new Client();
		// phpcs:disable
		$this->accountID         = get_option( 'pcs_poster_account_id', false );
		$this->applicationID     = get_option( 'pcs_poster_application_id', false );
		$this->applicationSecret = get_option( 'pcs_poster_application_secret', false );
		$this->token             = get_option( 'pcs_auth_token' );
		// phpcs:enable
	}

	/**
	 * Get token.
	 */
	public function getToken(): void {

		$url         = "https://$this->accountID.joinposter.com/api/v2/auth/access_token";
		$redirectUrl = get_bloginfo( 'url' ) . '/wp-admin/admin.php?page=pcs-settings';

		$auth['form_params']['application_id']     = $this->applicationID;
		$auth['form_params']['application_secret'] = $this->applicationSecret;
		$auth['form_params']['grant_type']         = 'authorization_code';
		$auth['form_params']['response_type']      = 'code';
		$auth['form_params']['redirect_uri']       = $redirectUrl;
		$auth['form_params']['code']               = ! empty( $_GET['code'] ) ? filter_var( wp_unslash( $_GET['code'] ), FILTER_SANITIZE_STRING ) : false;


		$data = $this->http->request( 'post', $url, $auth )->getBody()->getContents();
		try {
			$token = json_decode( $data, true, 512, JSON_THROW_ON_ERROR );
		} catch ( \JsonException $e ) {
			/**
			 * Error.
			 *
			 * @todo create error log
			 */
		}

		if ( ! empty( $token['access_token'] ) ) {
			update_option( 'pcs_auth_token', $token['access_token'] );
			add_action( 'admin_notices', 'PCS\Admin\AdminNotice::token_created' );
		}
	}

	/**
	 * Get Client Group
	 */
	public function getClientGroup() {
		$group = get_transient( 'pcs_user_groups' );

		if ( $group ) {
			return $group;
		}

		if ( ! $this->token ) {
			return;
		}

		$url = 'https://joinposter.com/api/clients.getGroups?token=' . $this->token;

		try {
			$data = json_decode(
				$this->http->request( 'get', $url )
					->getBody()
					->getContents(),
				false,
				512,
				JSON_THROW_ON_ERROR
			);
		} catch ( GuzzleException $e ) {
			/**
			 * Error.
			 *
			 * @todo create error log
			 */
		} catch ( \JsonException $e ) {
			/**
			 * Error.
			 *
			 * @todo create error log
			 */
		}

		if ( ! empty( $data->response ) ) {
			set_transient( 'pcs_user_groups', $data->response, DAY_IN_SECONDS );
		}
	}

	/**
	 * Get Users.
	 *
	 * @param int $groupID Group User ID.
	 * @param int $offset  Offset Number User.
	 *
	 * @return false|object
	 */
	public function getUsers( int $groupID, int $offset = 0 ) {
		$url = 'https://joinposter.com/api/clients.getClients?token=' . $this->token . '&group_id=' . $groupID . '&num=100&offset=' . $offset;
		try {
			$data = json_decode(
				$this->http->request( 'get', $url )
					->getBody()
					->getContents(),
				false,
				512,
				JSON_THROW_ON_ERROR
			);
		} catch ( GuzzleException $e ) {
			/**
			 * Error.
			 *
			 * @todo create error log
			 */
		} catch ( \JsonException $e ) {
			/**
			 * Error.
			 *
			 * @todo create error log
			 */
		}

		if ( ! empty( $data->response ) ) {
			return $data->response;
		}

		return false;
	}

	/**
	 * Checks if the user exists on the system.
	 *
	 * @param int $userID Wp user id.
	 *
	 * @return bool
	 */
	public function checkUserID( int $userID ): bool {
		global $wpdb;
		$prefix = $wpdb->prefix;

		// phpcs:disable
		$result = $wpdb->get_results( "SELECT * FROM {$prefix}pcs_poser_customer_sync WHERE `wp_u_ID` = {$userID}" );
		// phpcs:enable
		if ( ! empty( $result ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Create user in poster.
	 *
	 * @param array $userData User Data.
	 *
	 * @return false|int
	 * @throws \JsonException Json.
	 */
	public function createUser( array $userData ) {
		$url  = 'https://joinposter.com/api/clients.createClient?token=' . $this->token;
		$data = [];
		try {
			$data = json_decode(
				$this->http->request( 'post', $url, $userData )
					->getBody()
					->getContents(),
				false,
				512,
				JSON_THROW_ON_ERROR
			);
		} catch ( GuzzleException $e ) {
			/**
			 * Error.
			 *
			 * @todo create error log
			 */
		}

		if ( ! empty( $data->response ) ) {
			return $data->response;
		}

		return false;

	}

	/**
	 * Add User in Custom table.
	 * If the User is in this table, then he has already passed the synchronization and there is no need to add him
	 *
	 * @param int $userID   Poster User ID.
	 * @param int $wpUserID WP ID User.
	 *
	 * @return bool|int
	 */
	public function addCustomerToDb( int $userID, int $wpUserID ) {
		global $wpdb;
		$prefix = $wpdb->prefix;

		// phpcs:ignore WordPress.DB.DirectDatabaseQuery.DirectQuery
		return $wpdb->insert(
			"{$prefix}pcs_poser_customer_sync",
			[
				'sync'    => 1,
				'u_ID'    => $userID,
				'wp_u_ID' => $wpUserID,
			]
		);
	}

	/**
	 * Update user Payed Sum.
	 *
	 * @param int $userPosterUser Poster User ID.
	 * @param int $amount         Total sum order.
	 */
	public function updateUserTotal( int $userPosterUser, int $amount ) {

		$data                 = [];
		$url                  = 'https://joinposter.com/api/clients.changeClientPayedSum?token=' . $this->token;
		$prams['form_params'] = [
			'client_id' => $userPosterUser,
			'count'     => $amount,
		];
		try {
			$data = json_decode(
				$this->http->request( 'post', $url, $prams )
					->getBody()
					->getContents(),
				false,
				512,
				JSON_THROW_ON_ERROR
			);
		} catch ( GuzzleException $e ) {
			/**
			 * Error.
			 *
			 * @todo create error log
			 */
		} catch ( \JsonException $e ) {
			/**
			 * Error.
			 *
			 * @todo create error log
			 */
		}

		if ( ! empty( $data->response ) ) {
			return $data->response;
		}

		return false;

	}

	/**
	 * Get User Info to ID.
	 *
	 * @param int $posterUserID Poster User ID.
	 *
	 * @return mixed|bool
	 */
	public function getUser( int $posterUserID ) {

		if ( empty( $posterUserID ) ) {
			return false;
		}

		$url  = 'https://joinposter.com/api/clients.createClient?token=' . $this->token . '&client_id=' . $posterUserID;
		$data = [];
		try {
			$data = json_decode(
				$this->http->request( 'get', $url )
					->getBody()
					->getContents(),
				false,
				512,
				JSON_THROW_ON_ERROR
			);
		} catch ( GuzzleException $e ) {
			/**
			 * Error.
			 *
			 * @todo create error log
			 */
		} catch ( \JsonException $e ) {
			/**
			 * Error.
			 *
			 * @todo create error log
			 */
		}

		if ( ! empty( $data->response ) ) {
			return $data->response;
		}

		return false;
	}

	/**
	 * Get Poster User ID.
	 *
	 * @param int|string $userID WP User ID.
	 *
	 * @return array|false|object
	 */
	public function getPosterID( $userID ) {
		global $wpdb;
		$prefix = $wpdb->prefix;
		// phpcs:ignore WordPress.DB.DirectDatabaseQuery.DirectQuery
		// phpcs:ignore WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:ignore WordPress.DB.PreparedSQL.InterpolatedNotPrepared
		$result = $wpdb->get_results( "SELECT u_ID FROM {$prefix}pcs_poser_customer_sync WHERE `wp_u_ID` = {$userID}" );

		if ( ! empty( $result ) ) {
			return $result;
		}

		return false;
	}

	/**
	 * Checking if this user has been synced before.
	 *
	 * @param int $userID Poster User ID.
	 *
	 * @return bool
	 */
	public function getCustomerStatusSync( int $userID ): bool {
		global $wpdb;
		$prefix = $wpdb->prefix;

		// phpcs:disable
		$result = $wpdb->get_results( "SELECT * FROM {$prefix}pcs_poser_customer_sync WHERE `u_ID`= {$userID}" );
		// phpcs:enable
		if ( ! empty( $result ) ) {
			return true;
		}

		return false;
	}


	/**
	 * Send Credentials to site.
	 *
	 * @param string $email    Email.
	 * @param string $password Password.
	 *
	 * @return false|void
	 */
	public function sendEmailUsers( string $email, string $password ) {
		if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
			return false;
		}

		$headers = [
			'From: Coma.org.ua <' . get_option( 'admin_email' ) . '>',
			'content-type: text/html',
		];

		$message = "<p>Ваш логин: $email</p>";
		$message .= "<p>Ваш Пароль: $password</p>";

		return wp_mail( $email, __( 'You are registered on the Coma.org.ua website', 'pcs' ), $message, $headers );

	}

	/**
	 * Adds a discount card to a user.
	 *
	 * @param string $cardNumber      Discount card number.
	 * @param int    $discountPercent Percentage discount.
	 *
	 * @return bool
	 */
	public function createDiscountCard( string $cardNumber, int $discountPercent ) {
		$couponCode   = $cardNumber;
		$amount       = $discountPercent;
		$discountType = 'percent';

		$discount = [
			'post_title'   => $couponCode,
			'post_content' => '',
			'post_status'  => 'publish',
			'post_author'  => 1,
			'post_type'    => 'shop_coupon',
		];

		$newDiscountID = wp_insert_post( $discount );

		if ( is_wp_error( $newDiscountID ) ) {
			/**
			 * Error.
			 *
			 * @todo Add error log.
			 */
			return false;
		}

		update_post_meta( $newDiscountID, 'coupon_amount', $amount );
		update_post_meta( $newDiscountID, 'discount_type', $discountType );

		return true;
	}

}
