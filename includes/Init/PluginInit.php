<?php
/**
 * Created 12.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PCS\Init
 */

namespace PCS\Init;

use PCS\Admin\AdminNotice;
use PCS\Admin\SettingsPage;
use PCS\Cron\SyncCron;
use PCS\Woocommerce\InitActions;

/**
 * PluginInit Class file.
 */
class PluginInit {
	/**
	 * PluginInit construct.
	 */
	public function __construct() {
		new AdminNotice();
		new SettingsPage();
		new InitActions();
		new SyncCron();
		add_action( 'admin_enqueue_scripts', [ $this, 'add_admin_script' ] );
		add_action( 'admin_init', [ $this, 'create_custom_table' ] );
	}

	/**
	 * Check php version.
	 *
	 * @return bool
	 * @noinspection ConstantCanBeUsedInspection
	 */
	public static function is_php_version(): bool {
		if ( version_compare( constant( 'PCS_MINIMUM_PHP_REQUIRED_VERSION' ), phpversion(), '>' ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Return Version file.
	 *
	 * @param string $src The relative path to the file in the theme.
	 *
	 * @return false|int
	 */
	private static function version_file( string $src ) {
		return filemtime( PCS_PATH . $src );
	}

	/**
	 * Add Script and Style to admin panel.
	 *
	 * @param string $hook Hook name page in admin.
	 */
	public function add_admin_script( string $hook ): void {
		if ( 'toplevel_page_pcs-settings' === $hook ) {
			wp_enqueue_script( 'admin-bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js', [ 'jquery' ], '5.0.2', true );
			wp_enqueue_script(
				'admin-main',
				PCS_URL . '/assets/js/admin/main.js',
				[
					'jquery',
					'admin-bootstrap',
				],
				self::version_file( '/assets/js/admin/main.js' ),
				true
			);

			wp_localize_script(
				'admin-main',
				'pcs',
				[
					'url'   => admin_url( 'admin-ajax.php' ),
					'nonce' => wp_create_nonce( 'pcs_ajax_nonce' ),
				]
			);

			wp_enqueue_style(
				'bootstrap-admin',
				'//cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css',
				'',
				'5.0.2'
			);

			wp_enqueue_style( 'admin-style', PCS_URL . '/assets/css/style.css', '', self::version_file( '/assets/css/style.css' ) );
		}
	}

	/**
	 * Create Custom table poser_customer_sync.
	 */
	public function create_custom_table(): void {
		global $wpdb;

		$prefix = $wpdb->base_prefix;
		// phpcs:disable WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
		// phpcs:disable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
		$table = $wpdb->get_results( "SHOW TABLES LIKE '{$prefix}pcs_poser_customer_sync'" );

		if ( ! $table ) {
			$sql = "CREATE TABLE `{$prefix}pcs_poser_customer_sync` ( `ID` BIGINT NOT NULL AUTO_INCREMENT ,  `sync` BOOLEAN NOT NULL DEFAULT FALSE , `u_ID` BIGINT NULL, `wp_u_ID` BIGINT NULL, PRIMARY KEY (`ID`));";

			include_once ABSPATH . 'wp-admin/includes/upgrade.php';

			dbDelta( $sql );
		}
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery
		// phpcs:enable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
	}
}
