<?php
/**
 * Created 19.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PCS\Woocommerce
 */

namespace PCS\Woocommerce;

use PCS\Poster\Api;

/**
 * InitActions class file.
 */
class InitActions {
	/**
	 * Class Api posetr.
	 *
	 * @var Api Api Poster.
	 */
	private $api;

	/**
	 * InitActions construct.
	 */
	public function __construct() {
		$this->api = new Api();
		add_action( 'woocommerce_new_order', [ $this, 'new_order' ] );
		add_action( 'woocommerce_order_status_completed', [ $this, 'change_status_complete' ] );
	}

	/**
	 * New Order Create user in poster.
	 *
	 * @param int $order_id Order ID.
	 *
	 * @throws \JsonException JSON.
	 */
	public function new_order( $order_id ): void {
		$order = wc_get_order( $order_id );

		if ( ! $this->api->check_user_ID( $order->get_customer_id() ) ) {

			$new_client['form_params'] = [
				'client_name'             => filter_var( $order->get_billing_first_name() . $order->get_billing_last_name(), FILTER_SANITIZE_STRING ),
				'client_sex'              => 0,
				'client_groups_id_client' => 1,
				'card_number'             => '',
				'discount_per'            => 0,
				'phone'                   => $order->get_billing_phone(),
				'email'                   => $order->get_billing_email(),
			];

			$user_id = $this->api->create_user( $new_client );

			if ( $user_id ) {
				$this->api->add_customer_to_db( $user_id, $order->get_customer_id() );
			}
		}
	}

	/**
	 * When the status is changed, the value of all purchases of the user in the system is updated poster.
	 *
	 * @param int|string $order_id WooCommerce Order ID.
	 */
	public function change_status_complete( $order_id ): void {
		$order = wc_get_order( $order_id );
		$total = $order->get_total();

		if ( $this->api->check_user_ID( $order->get_customer_id() ) ) {
			$poster_user_id = $this->api->get_poster_ID( $order->get_customer_id() );

			if ( $poster_user_id ) {
				$updateUserTotal = $this->api->update_user_total( $poster_user_id[0]->u_ID, $total );
			}

		}
	}
}
