<?php
/**
 * Created 12.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PCS\Admin
 */

namespace PCS\Admin;

/**
 * AdminNotice Class file.
 */
class AdminNotice {
	/**
	 * Low Version PHP.
	 */
	public static function php_version_nope(): void {
		printf(
			'<div id="pcs-php-nope" class="notice notice-error is-dismissible"><p>%s</p></div>',
			wp_kses(
				sprintf(
				/* translators: 1: Required PHP version number, 2: Current PHP version number, 3: URL of PHP update help page */
					__( 'The Poster Customer Sync plugin requires PHP version %1$s or higher. This site is running PHP version %2$s. <a href="%3$s">Learn about updating PHP</a>.', 'pcs' ),
					PCS_MINIMUM_PHP_REQUIRED_VERSION,
					PHP_VERSION,
					'https://wordpress.org/support/update-php/'
				),
				[
					'a' => [
						'href' => [],
					],
				]
			)
		);
	}

	/**
	 * Activation Woocommerce Plugin.
	 */
	public static function woocommerce_active(): void {
		printf(
			'<div id="pcs-woocommerce-nope" class="notice notice-warning is-dismissible"><p>%s</p></div>',
			esc_html(
				__( 'For Poster Customer Sync to work, you need to activate the Woocommerce Plugin', 'pcs' ),
			)
		);
	}

	/**
	 * Token is empty.
	 *
	 * @return void
	 */
	public static function application_token_empty(): void {
		printf(
			'<div id="pcs-php-nope" class="notice notice-error is-dismissible"><p>%s</p></div>',
			esc_html__( 'You save an empty token key, go to the documentation.', 'pcs' )
		);
	}

	/**
	 * Bad nonce error.
	 *
	 * @return void
	 */
	public static function bad_nonce_code(): void {
		printf(
			'<div id="pcs-php-nope" class="notice notice-error is-dismissible"><p>%s</p></div>',
			esc_html__( 'Nonce code is bad', 'pcs' )
		);
	}

	/**
	 * Account secret key is empty.
	 */
	public static function credence_empty(): void {
		printf(
			'<div id="pcs-woocommerce-nope" class="notice notice-warning is-dismissible"><p>%s</p></div>',
			esc_html(
				__( 'Fill in the section first Authorization Credentials', 'pcs' ),
			)
		);
	}

	/**
	 * Account secret key is empty.
	 */
	public static function token_created(): void {
		printf(
			'<div id="pcs-woocommerce-nope" class="notice notice-success is-dismissible"><p>%s</p></div>',
			esc_html(
				__( 'You have successfully logged in to the system', 'pcs' ),
			)
		);
	}

	/**
	 * Unknown error.
	 */
	public static function error(): void {
		printf(
			'<div id="pcs-woocommerce-nope" class="notice notice-error is-dismissible"><p>%s</p></div>',
			esc_html(
				__( 'Unknown error contact the plugin developer', 'pcs' ),
			)
		);
	}

}
