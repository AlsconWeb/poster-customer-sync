<?php
/**
 * Created 14.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PCS\Admin
 */

namespace PCS\Admin;

use GuzzleHttp\Exception\GuzzleException;
use PCS\Poster\Api;

/**
 * SettingsPage class file.
 */
class SettingsPage {
	/**
	 * Api.
	 *
	 * @var Api Api class poster.
	 */
	private $api;

	/**
	 * Construct class SettingsPage.
	 */
	public function __construct() {
		add_action( 'admin_menu', [ $this, 'registerSettingsPage' ] );
		add_action( 'admin_init', [ $this, 'savePosterCredence' ] );
		add_action( 'admin_init', [ $this, 'getAccessesCode' ] );

		add_action( 'wp_ajax_sync_user_group', [ $this, 'userGroupSync' ] );
		add_action( 'wp_ajax_get_progress', [ $this, 'getProgress' ] );

		$this->api = new Api();
	}

	/**
	 * Register Settings Page.
	 */
	public function registerSettingsPage(): void {
		add_menu_page(
			__( 'Settings', 'pcs' ),
			__( 'Settings Poster', 'pcs' ),
			'manage_options',
			'pcs-settings',
			[
				$this,
				'addSettingsPage',
			],
			'dashicons-admin-generic',
			90
		);
	}

	/**
	 * Output HTML settings page.
	 */
	public function addSettingsPage(): void {
		ob_start();
		include_once PCS_PATH . '/template/admin/settingPage.php';
		// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		echo ob_get_clean();
		// phpcs:enable WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/**
	 * Add Poster Credence Account.
	 */
	public function savePosterCredence(): void {
		if ( ! isset( $_POST['pcs_auth'] ) ) {
			return;
		}

		check_admin_referer( 'pcs_auth', 'pcs_auth_nonce' );

		$accountID         = filter_input( INPUT_POST, 'pcs_account_id', FILTER_SANITIZE_STRING );
		$applicationID     = filter_input( INPUT_POST, 'pcs_application_id', FILTER_SANITIZE_STRING );
		$applicationSecret = filter_input( INPUT_POST, 'pcs_secret_key', FILTER_SANITIZE_STRING );

		if ( empty( $accountID ) ) {
			add_action( 'admin_notices', 'PCS\Admin\AdminNotice::account_id_empty' );
		}

		if ( empty( $applicationID ) ) {
			add_action( 'admin_notices', 'PCS\Admin\AdminNotice::application_id_empty' );
		}

		if ( empty( $applicationSecret ) ) {
			add_action( 'admin_notices', 'PCS\Admin\AdminNotice::application_secret_empty' );
		}

		update_option( 'pcs_poster_account_id', $accountID, 'yes' );
		update_option( 'pcs_poster_application_id', $applicationID, 'yes' );
		update_option( 'pcs_poster_application_secret', $applicationSecret, 'yes' );

	}

	/**
	 * Get Token Authorization.
	 */
	public function getAccessesCode(): void {

		$this->getTokenCode();

		try {
			$this->api->getClientGroup();
		} catch ( \JsonException $e ) {
			add_action( 'admin_notices', 'PCS\Admin\AdminNotice::error' );
		}

		if ( ! isset( $_POST['pcs_get_token'] ) ) {
			return;
		}

		check_admin_referer( 'pcs_get_token', 'pcs_get_token_nonce' );

		$accountID         = get_option( 'pcs_poster_account_id', false );
		$applicationID     = get_option( 'pcs_poster_application_id', false );
		$applicationSecret = get_option( 'pcs_poster_application_secret', false );

		if ( empty( $accountID ) || empty( $applicationID ) || empty( $applicationSecret ) ) {
			add_action( 'admin_notices', 'PCS\Admin\AdminNotice::credence_empty' );

			return;
		}

		$redirectUrl = get_bloginfo( 'url' ) . '/wp-admin/admin.php?page=pcs-settings';

		$url = "https://$accountID.joinposter.com/api/auth?application_id=$applicationID&redirect_uri=$redirectUrl&response_type=code";

		// phpcs:ignore WordPress.Security.SafeRedirect.wp_redirect_wp_redirect
		wp_redirect( $url );
	}

	/**
	 * Get token.
	 */
	public function getTokenCode(): void {

		if ( ! isset( $_GET['code'] ) ) {
			return;
		}

		try {
			$this->api->getToken();
		} catch ( \JsonException $e ) {
			add_action( 'admin_notices', 'PCS\Admin\AdminNotice::error' );
		}
	}


	/**
	 * Ajax Handler User Sync.
	 */
	public function userGroupSync(): void {

		check_ajax_referer( 'pcs_ajax_nonce', 'nonce' );

		$groupID   = ! empty( $_POST['groupID'] ) ? filter_var( wp_unslash( $_POST['groupID'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;
		$userCount = ! empty( $_POST['userCount'] ) ? filter_var( wp_unslash( $_POST['userCount'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;

		if ( 0 === $groupID ) {
			wp_send_json_error( [ 'message' => __( 'Select a user group to sync', 'pcs' ) ] );
		}

		if ( 0 === $userCount ) {
			wp_send_json_error( [ 'message' => __( 'There are no users in this group', 'pcs' ) ] );
		}

		$pagesNumber = ceil( $userCount / 100 );

		if ( 0 === (int) $pagesNumber ) {
			$pagesNumber = 1;
		}

		delete_transient( 'user_catered' );
		delete_transient( 'skip_catered' );

		$userCounts = 0;
		$skipUser   = 0;

		for ( $i = 0; $i < $pagesNumber; $i ++ ) {
			$page = $i * 100;

			$users = (object) [];
			try {
				$users = $this->api->getUsers( $groupID, $page );
			} catch ( GuzzleException $e ) {
				wp_send_json_error( [ 'message' => $e->getMessage() ] );
			}

			foreach ( $users as $user ) {

				$userStatus = $this->createCustomer( $user );

				if ( $userStatus ) {
					$userCounts = (int) ( get_transient( 'user_catered' ) ?? 0 );
					$userCounts ++;
					set_transient( 'user_catered', $userCounts, HOUR_IN_SECONDS );
				}

				if ( ! $userStatus ) {
					$skipUser = (int) ( get_transient( 'skip_catered' ) ?? 0 );
					$skipUser ++;
					set_transient( 'skip_catered', $skipUser, HOUR_IN_SECONDS );
				}
			}
		}

		wp_send_json_success(
			[
				'message' => __( 'Synchronization completed successfully', 'pcs' ),
				'create'  => 0 === $userCounts ? $userCounts : $userCounts + 1,
				'skip'    => 0 === $skipUser ? $skipUser : $skipUser + 1,
			]
		);
	}

	/**
	 * Get Progress in sync process.
	 */
	public function getProgress(): void {
		check_ajax_referer( 'pcs_ajax_nonce', 'nonce' );

		$userCounts = (int) ( get_transient( 'user_catered' ) ?? 0 );
		$skipUser   = (int) ( get_transient( 'skip_catered' ) ?? 0 );

		wp_send_json_success(
			[
				'create'     => __( 'Created: ', 'pcs' ) . ( ! $userCounts ? 0 : $userCounts + 1 ),
				'skip'       => __( 'Skip: ', 'pcs' ) . ( ! $skipUser ? 0 : $skipUser + 1 ),
				'create_num' => ( ! $userCounts ? 0 : $userCounts + 1 ),
				'skip_num'   => ( ! $skipUser ? 0 : $skipUser + 1 ),
			]
		);
	}

	/**
	 * Create Customer.
	 *
	 * @param object $user User Object from API.
	 *
	 * @return bool
	 */
	public function createCustomer( object $user ): bool {
		if ( empty( $user->email ) ) {
			return false;
		}

		if ( $this->api->getCustomerStatusSync( $user->client_id ) ) {
			return false;
		}

		$password = wp_generate_password( 10, false, false );
		$userData = [
			'user_login'      => $user->email,
			'user_pass'       => $password,
			'user_email'      => $user->email,
			'first_name'      => $user->firstname,
			'last_name'       => $user->lastname,
			'admin_color'     => 'fresh',
			'user_registered' => current_time( 'Y-m-d H:i:s' ),
			'role'            => 'customer',
		];

		$userID = wp_insert_user( $userData );
		if ( is_wp_error( $userID ) ) {
			return false;
		}

		$this->api->sendEmailUsers( $user->email, $password );
		$this->api->addCustomerToDb( $user->client_id, $userID );

		if ( ! empty( $user->card_number ) ) {
			$this->api->createDiscountCard( (string) $user->card_number, (int) $user->client_groups_discount );
		}

		update_user_meta( $userID, 'billing_first_name', $user->firstname );
		update_user_meta( $userID, 'billing_last_name', $user->lastname );
		update_user_meta( $userID, 'pcs_discount_cart', $user->card_number );
		update_user_meta( $userID, 'billing_phone', $user->phone_number );
		update_user_meta( $userID, 'pcs_total_payed_sum', $user->total_payed_sum );
		update_user_meta( $userID, 'pcs_poster_user_id', $user->client_id );

		return true;
	}
}

