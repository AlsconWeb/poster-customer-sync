<?php
/**
 * Created 20.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PCS\Cron
 */

namespace PCS\Cron;

use GuzzleHttp\Exception\GuzzleException;
use PCS\Poster\Api;

/**
 * SyncCron Class file.
 */
class SyncCron {

	/**
	 * API.
	 *
	 * @var Api API Posetr Class.
	 */
	private $api;

	/**
	 * SyncCron construct.
	 */
	public function __construct() {
		$this->api = new Api();
		add_action( 'update_new_customer', [ $this, 'syncNewCustomer' ] );
	}


	/**
	 * Sync Cron New User.
	 */
	public function syncNewCustomer(): void {
		$groupUser = $this->api->getClientGroup();

		if ( ! empty( $groupUser ) ) {
			foreach ( $groupUser as $group ) {
				$groupID   = $group->client_groups_id;
				$userCount = $group->count_groups_clients;

				$pagesNumber = ceil( $userCount / 100 );

				if ( 0 === (int) $pagesNumber ) {
					$pagesNumber = 1;
				}

				for ( $i = 0; $i < $pagesNumber; $i ++ ) {
					$page = $i * 100;

					$users = (object) [];
					try {
						$users = $this->api->getUsers( $groupID, $page );
					} catch ( GuzzleException $e ) {
						/**
						 * Error.
						 *
						 * @todo Error logs.
						 */
					}

					foreach ( $users as $user ) {
						$this->createCustomer( $user );
					}
				}
			}
		}

	}

	/**
	 * Create Customer.
	 *
	 * @param object $user User Object from API.
	 *
	 * @return bool
	 */
	public function createCustomer( object $user ): bool {
		if ( empty( $user->email ) ) {
			return false;
		}

		if ( $this->api->getCustomerStatusSync( $user->client_id ) ) {
			return false;
		}

		$password = wp_generate_password( 10, false, false );
		$userData = [
			'user_login'      => $user->email,
			'user_pass'       => $password,
			'user_email'      => $user->email,
			'first_name'      => $user->firstname,
			'last_name'       => $user->lastname,
			'admin_color'     => 'fresh',
			'user_registered' => current_time( 'Y-m-d H:i:s' ),
			'role'            => 'customer',
		];

		$userID = wp_insert_user( $userData );
		if ( is_wp_error( $userID ) ) {
			return false;
		}

		$this->api->sendEmailUsers( $user->email, $password );
		$this->api->addCustomerToDb( $user->client_id, $userID );
		if ( ! empty( $user->card_number ) ) {
			$this->api->createDiscountCard( (string) $user->card_number, (int) $user->client_groups_discount );
		}

		update_user_meta( $userID, 'billing_first_name', $user->firstname );
		update_user_meta( $userID, 'billing_last_name', $user->lastname );
		update_user_meta( $userID, 'pcs_discount_cart', $user->card_number );
		update_user_meta( $userID, 'billing_phone', $user->phone_number );
		update_user_meta( $userID, 'pcs_total_payed_sum', $user->total_payed_sum );
		update_user_meta( $userID, 'pcs_poster_user_id', $user->client_id );

		return true;
	}
}